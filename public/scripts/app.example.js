class App {
  constructor() {
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.tipeDriver = document.getElementById("tipeDriver").value;
    this.tanggal = document.getElementById("tanggal").value;
    this.waktu = document.getElementById("waktu").value;
    this.jumlahPenumpang = document.getElementById("jumlahPenumpang").value;
  }

  init() {
    // Register click listener
    this.loadButton.onclick = this.run;
    this.load();
  }

  run = () => {
    let year = this.tanggal.split("-");
    let newCars = [];
    Car.list.forEach((car) => {
      if (this.tanggal === "") {
        if (car.capacity == this.jumlahPenumpang ) {
          newCars.push(car);
        }
      }
      if (this.jumlahPenumpang === "") {
        if (car.year == year[0] ) {
          newCars.push(car);
        }
      }
      if (car.year == year[0] && car.capacity == this.jumlahPenumpang ) {
        newCars.push(car);
      } 
    });
    if (newCars.length == 0) {
      this.carContainerElement.innerHTML = `
        <h1 class="text-center m-3">Mobil tidak tersedia</h1>
      `
    } else {
      newCars.forEach((car) => {
        const node = document.createElement("div");
        node.classList.add('card', 'p-3', 'my-4', 'list-card');
        node.innerHTML = car.render();
        this.carContainerElement.appendChild(node);
      });
    }
    
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }
}
