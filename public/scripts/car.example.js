class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <img src="${this.image}" alt="${this.manufacture}" width="300px" height="200px">
      <p class="card-text text-dark">${this.type}</p>
      <p class="card-text"><strong>Rp ${this.rentPerDay} / hari</strong></p>
      <p class="card-text text-justify">${this.description}</p>
      <p class="card-text"><i class="bi bi-people"></i> ${this.capacity} orang</p>
      <p class="card-text"><i class="bi bi-gear"></i> ${this.transmission}</p>
      <p class="card-text"><i class="bi bi-calendar"></i> Tahun ${this.year}</p>
      <button class="btn btn-success">Pilih Mobil</button>
    `;
  }
}
