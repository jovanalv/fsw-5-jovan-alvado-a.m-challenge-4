const http = require('http');
const { PORT = 3000 } = process.env;
const fs = require('fs');
const path = require('path');

function onRequest(req, res) {
    if (req.url === '/') {
        fs.readFile("./public/index.html", "UTF-8", function(err, html){
            res.writeHead(200, {"Content-Type": "text/html"});
            res.end(html);
        });
    } else if (req.url === '/cars') {
        fs.readFile("./public/cars.html", "UTF-8", function(err, html){
            res.writeHead(200, {"Content-Type": "text/html"});
            res.end(html);
        });
    } else if (req.url.match('\.css$')) {
        let cssPath = path.join(__dirname, 'public', req.url);
        let fileStream = fs.createReadStream(cssPath, 'UTF-8');
        res.writeHead(200, {"Content-Type": "text/css"});
        fileStream.pipe(res)
    } else if (req.url.match('\.js$')) {
        let jsPath = path.join(__dirname, 'public', req.url);
        let fileStream = fs.createReadStream(jsPath, 'UTF-8');
        res.writeHead(200, {"Content-Type": "text/javascript"});
        fileStream.pipe(res)
    } else if (req.url.match('\.jpg$')) {
        let jpgPath = path.join(__dirname, 'public', req.url);
        let fileStream = fs.createReadStream(jpgPath);
        res.writeHead(200, {"Content-Type": "image/jpg"});
        fileStream.pipe(res)
    } else if (req.url.match('\.svg$')) {
        let svgPath = path.join(__dirname, 'public', req.url);
        let fileStream = fs.createReadStream(svgPath);
        res.writeHead(200, {"Content-Type": "image/svg"});
        fileStream.pipe(res)
    } else if (req.url.match('\.png$')) {
        let pngPath = path.join(__dirname, 'public', req.url);
        let fileStream = fs.createReadStream(pngPath);
        res.writeHead(200, {"Content-Type": "image/png"});
        fileStream.pipe(res)
    } else {
        res.writeHead(404, {"Content-Type": "text/html"});
        res.end("No Page Found");
    }
}

const server = http.createServer(onRequest);

server.listen(PORT, '127.0.0.1', () => {
    console.log(`Server berjalan di http://127.0.0.1:${PORT}`);
})